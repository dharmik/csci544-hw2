About:
The scripts in this repository classifies the feature vectors to their respective classes using percerptrons.

Applied Natural Language Processing (CSCI 544) Home work - 2.

Author: Dharmik

Date: Feb 24th, 2015

Contents:
perceplearn.py
    This script trains the perceptrons for a given TRAININGFILE and writes the learned model to a MODELFILE passed as arguments.
    Usage: python3 perceplearn.py TRAININGFILE MODELFILE [-h DEVFILE]
    
percepclassify.py
    This file classifies the sentences from STDIN using the perceptron from MODELFILE and prints them to STDOUT.
    Usage: cat INPUTFILE | python3 percepclassify.py MODELFILE > OUTPUTFILE

postagging/postrain.py
    This file formats the given Parts-of-Speech (POS) training file TRAININGFILE and calls on the perceplearn.py to train the perceptrons and write the model learned to MODELFILE.
    Usage: python3 postrain.py TRAININGFILE MODEL [-h DEVFILE]
    
postagging/postag.py
    This file classifies the sentences from STDIN using the perceptron from MODELFILE and prints them to STDOUT.
    Usage: cat INPUTFILE | python3 postag.py MODELFILE > OUTPUTFILE
    
ner/nelearn.py
    This file formats the given Named-Entity Recognition (NER) training file TRAININGFILE and calls on the perceplearn.py to train the perceptrons and write the model learned to MODELFILE.
    Usage: python3 nelearn.py TRAININGFILE MODEL [-h DEVFILE]
    
ner/netag.py
    This file classifies the sentences from STDIN using the perceptron from MODELFILE and prints them to STDOUT.
    Usage: cat INPUTFILE | python3 netag.py MODELFILE > OUTPUTFILE
    
PART - IV

1) What is the accuracy of your part-of-speech tagger?
A) Accuracy of POS = 0.9506692923199641


2) What are the precision, recall and F-score for each of the named entity types for your named entity recognizer, and what is the overall F-score?

A) CLASS PRECISION       RECALL             F-SCORE

   B-PER   1.0      0.4098360655737705 0.5813953488372093

   B-MISC  1.0      0.4368131868131868 0.6080305927342257

   B-ORG   1.0      0.5268630849220104 0.6901248581157775

   B-LOC   1.0      0.4095371669004207 0.581094527363184
   
   Overall Average F-score: 0.62


3) What happens if you use your Naive Bayes classifier instead of your perceptron classifier (report performance metrics)? Why do you think that is?
A) The accuracy of Naive Bayes classifier is less than that of perceptron classifier because of the multi dimentional feature vector. The multi-dimensional vector provides the context for POS tagging which the Naive Bayes disregards and thus the accuracy will take a hit in Naive Bayes.
NB Accuracy = 0.84944038686841
Perceptron accuracy = 0.9506692923199641
