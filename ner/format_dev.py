import sys
from os.path import basename

def main(ner_dev):
    f_in = open(ner_dev, "r", encoding="latin-1")
    op_file = "tagged_" + basename(ner_dev)
    f_out = open(op_file, "w", encoding="latin-1")
    for line in f_in.readlines():
        tagged_words = line.strip().split(" ")
        tags = []
        for w in tagged_words:
            word_list = w.rsplit("/", 2)
            tags.append(word_list[2])
        f_out.write(" ".join(tags) + "\n")
    f_out.close()
    f_in.close()

if __name__ == "__main__":
    main(sys.argv[1])
