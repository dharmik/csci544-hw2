"""
This file formats the given NER (Named Entity Recognition) training file
TRAININGFILE to a format which can be processed by perceplearn.py to train the
perceptrons and writes the model learned to a MODEL file specified.

Author: Dharmik.
Date: Feb 22, 2015.
"""

import getopt
import re
import sys
from subprocess import call
from os.path import basename

def get_word_shape(word):
    word = re.sub("[a-z]", "a", word)
    word = re.sub("[A-Z]", "A", word)
    word = re.sub("[1-9]", "1", word)
    word = re.sub("[^A-Za-z1-9]", "-", word)
    word = re.sub("(a+)", "a", word)
    word = re.sub("(A+)", "A", word)
    word = re.sub("(1+)", "1", word)
    word = re.sub("(-+)", "-", word)
    return word

def format_training_file(train_file):
    f_in = open(train_file, "r", encoding="latin-1")
    fmtd_output = "fmtd_" + basename(train_file)
    f_out = open(fmtd_output, "w", encoding="latin-1")
    for line in f_in.readlines():
        line_content = line.strip().split(" ")
        line_content.insert(0, "BOS/BOS/BOS")
        line_content.append("EOS/EOS/EOS")
        for i in range(1, len(line_content) - 1):
            prev_w, prev_tag, prev_ner = line_content[i-1].strip().rsplit("/", 2)
            cur_w, cur_tag, cur_ner = line_content[i].strip().rsplit("/", 2)
            next_w, next_tag, next_ner = line_content[i+1].strip().rsplit("/", 2)
            #f_out.write(cur_ner + " prev_" + prev_w + " cur_" + cur_w + " next_" + next_w + " prener_" + prev_ner +
            #            " pretag_" + prev_tag + " curtag_" + cur_tag + " nexttag_" + next_tag + "\n")
            #shape = get_word_shape(cur_w)
            f_out.write(cur_ner + " cur_" + cur_w + " prener_" + prev_ner +
                        " pretag_" + prev_tag + " curtag_" + cur_tag + " nexttag_" + next_tag + 
                        " seq_" + prev_tag + "_" + cur_tag + "_" + next_tag + "_" + prev_ner + " shape_" + get_word_shape(cur_w) + "\n")
    f_out.close()
    f_in.close()
    return fmtd_output


def main(train_file, model_file, argv):
    try:
        opts, args = getopt.getopt(argv, "h:i:", ["dev=", "iterations="])
    except getopt.GetoptError:
        print ("USAGE: python3 nelearn.py TRAININGFILE MODEL [-h DEVFILE -i ITERATIONS]")
        exit()
    dev_file = None
    for opt, arg in opts:
        if opt in ("-h", "--dev"):
            dev_file = arg
            
    # Format the NER training file to feed to perceptron.
    fmtd_train_file = format_training_file(train_file)
    
    # Format the dev file, if available.
    opt_dev_file_arg = ""
    if dev_file is not None:
        fmtd_dev_file = format_training_file(dev_file)
        opt_dev_file_arg = " -h " + fmtd_dev_file
    
    # Run the perceplearn on the formatted training file.
    try:
        retcode = call("python3 ../perceplearn.py " + fmtd_train_file +
                       " " + model_file + opt_dev_file_arg, shell=True)
        if retcode < 0:
            print ("Child was terminated by signal-", retcode)
            
    except OSError as e:
        print ("Execution of perceplearn failed:", e)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print ("USAGE: python3 nelearn.py TRAININGFILE MODEL [-h DEVFILE -i ITERATIONS]")
        exit()
    main(sys.argv[1], sys.argv[2], sys.argv[3:])
