import sys

def main(file1, file2, cls):
    classes = ["B-LOC", "B-MISC", "B-ORG", "B-PER"]
    prec = {}
    recall = {}
    for c in classes:
        prec.setdefault(c, 0)
        recall.setdefault(c, 0)
    #print (prec)
    #print (recall)
    #exit()
    f1 = open(file1, "r")
    f1_lines = [l for l in f1.readlines()]
    f2 = open(file2, "r")
    f2_lines = [l for l in f2.readlines()]
    #cls = "B-ORG"
    total = 0
    tp = 0
    fp = 0
    fn = 0
    for j in range(len(f1_lines)):
        # Compare line by line.
        words1 = f1_lines[j].strip().split()
        words2 = f2_lines[j].strip().split()
        flag = 0
        line_tags = []
        seq = []
        for i in range(len(words1)):
            if words1[i] == cls:
                flag = 1
                seq.append((words1[i], i, j))
            elif flag == 1 and not words1[i].startswith("B-"):
                seq.append((words1[i], i, j))
            else:
                if len(seq) > 0:
                    line_tags.append(seq)
                seq = []
                flag = 0
                if words1[i].startswith(cls):
                    flag = 1
                    seq.append((words1[i], i, j))
        if flag == 1 and len(seq) > 0:
            line_tags.append(seq)
        #if len(line_tags) > 0:
        #    print (line_tags)
        line_tags2 = []
        seq2 = []
        flag2 = 0
        for k in range(len(words2)):
            if words2[k] == cls:
                flag2 =1
                seq2.append((words2[k], k, j))
            elif flag2 == 1 and not words2[i].startswith(cls):
                seq2.append((words2[k], k, j))
            else:
                if len(seq2) > 0:
                    line_tags2.append(seq2)
                seq2 = []
                flag2 = 0
                if words2[i].startswith(cls):
                    flag = 1
                    seq.append((words2[i], i, j))
        if flag == 1 and len(seq) > 0:
            line_tags2.append(seq)
        
        # Compare the line_tags
        if len(line_tags) > 0 and len(line_tags2) <=0:
            fn += len(line_tags)
        elif len(line_tags) <= 0 and len(line_tags2) > 0:
            fp += len(line_tags2)
        elif len(line_tags) > 0 and len(line_tags2) > 0:
            for seq in line_tags:
                if seq in line_tags2:
                    tp += 1
                    line_tags2.remove(seq)
                else:
                    fn += 1
            fp += len(line_tags2)
    print ("TP = ", str(tp))
    print ("FP = ", str(fp))
    print ("FN = ", str(fn))
    prec = float(tp)/(float(tp) + float(fp))
    recall = float(tp)/(float(tp)+float(fn))
    print ("PREC", prec)
    print ("Recall=",recall)
    fs = 2*float(prec)*float(recall)/(prec + recall)
    print ("F=", fs)
                    
            

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3])
