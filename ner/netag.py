"""
This file takes the sentences to be NER tagged as input from STDIN,
tags the sentence using the perceptron from MODELFILE and prints them
to STDOUT.

USAGE: python3 netag.py MODELFILE

Author: Dharmik.
Date: Feb 23, 2015.
"""

import codecs
import json
import re
import sys

def get_word_shape(word):
    word = re.sub("[a-z]", "a", word)
    word = re.sub("[A-Z]", "A", word)
    word = re.sub("[1-9]", "1", word)
    word = re.sub("[^A-Za-z1-9]", "-", word)
    word = re.sub("(a+)", "a", word)
    word = re.sub("(A+)", "A", word)
    word = re.sub("(1+)", "1", word)
    word = re.sub("(-+)", "-", word)
    return word

def get_class(weights, word_list):
    """
    Calulates the possibility that a word belongs to partical class,
    and return that class with maximum weight.
    @param weights: Dict containing the weights of each feature for each class.
    @param word_list: List of words(n-gram) for which we have to calculate the class it belongs.
    @return: class to which the word belongs.
    """
    scores = {}
    for w in word_list:
        for cls in weights.get(w,[]):
            if cls not in scores:
                scores[cls] = weights[w][cls]
            else:
                scores[cls] += weights[w][cls]
    max_score = 1
    max_cls = "O"
    for cls in scores:
        if max_score <= scores[cls]:
            max_score = scores[cls]
            max_cls = cls
    #return (max_cls, max_score)
    return max_cls

def get_tagged_output(words, weights_avg):
    tagged_words = []
    words.insert(0, "BOS/BOS")
    words.append("EOS/EOS")
    ner_tag = "BOS"
    #f_out.write(cur_ner + " cur_" + cur_w + " prener_" + prev_ner +
    #            " pretag_" + prev_tag + " curtag_" + cur_tag + " nexttag_" + next_tag + " shape_" + get_word_shape(cur_w) + "\n")
    for i in range(1, len(words) - 1):
        prev_w, prev_pos = words[i-1].strip().rsplit("/", 1)
        cur_w, cur_pos = words[i].strip().rsplit("/", 1)
        next_w, next_pos = words[i+1].strip().rsplit("/", 1)
        features = ["cur_" + cur_w, "prener_" + ner_tag, "pretag_" + prev_pos,
                    "curtag_" + cur_pos, "seq_" + prev_pos + "_" + cur_pos + "_" + next_pos + "_" + ner_tag,
                    "nexttag_" + next_pos, "shape_" + get_word_shape(cur_w)]
        #print (features)
        ner_tag = get_class (weights_avg, features)
        tagged_words.append(words[i] + "/" + ner_tag)
    return tagged_words

def main(model_file):
    # Read model file.
    f_in = open(model_file, "r")
    weights_avg = f_in.readline()
    weights_avg = json.loads(weights_avg)

    # Read the lines from input file and classify them.
    #sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
    sys.stdin = codecs.getreader('latin-1')(sys.stdin.detach(), errors='ignore')
    for line in sys.stdin:
        words = line.strip().split(" ")
        out_line = get_tagged_output(words, weights_avg)
        #print(len(words))
        print (" ".join(out_line))
    

if __name__ == "__main__":
    main(sys.argv[1])
