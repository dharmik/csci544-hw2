"""
This file takes the sentences to be tagged as input from STDIN,
tags the sentence using the perceptron from MODELFILE and prints
them to STDOUT.

USAGE: python3 percepclassify.py MODELFILE

Author: Dharmik.
Date: Feb 24, 2015.
"""
import codecs
import json
import sys


def get_class(weights, word_list, default_cls):
    """
    Calulates the possibility that a word belongs to partical class,
    and return that class with maximum weight.
    @param weights: Dict containing the weights of each feature for each class.
    @param word_list: List of words(n-gram) for which we have to calculate the class it belons.
    @return: class to which the word belongs.
    """
    scores = {}
    for w in word_list:
        for cls in weights.get(w,[]):
            if cls not in scores:
                scores[cls] = weights[w][cls]
            else:
                scores[cls] += weights[w][cls]
    max_score = 1
    max_cls = default_cls
    for cls in scores:
        if max_score <= scores[cls]:
            max_score = scores[cls]
            max_cls = cls
    #return (max_cls, max_score)
    return max_cls

def main(model_file):
    # Read model file.
    f_in = open(model_file, "r")
    weights_avg = f_in.readline()
    weights_avg = json.loads(weights_avg)
    default_cls = f_in.readline()
    default_cls = default_cls.strip()
    
    # Read the lines from input file and classify them.
    sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
    for line in sys.stdin:
        words = line.strip().split(" ")
        cls = get_class(weights_avg, words, default_cls)
        print (cls)

if __name__ == "__main__":
    main(sys.argv[1])
