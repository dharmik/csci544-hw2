"""
This script trains the perceptrons for a given TRAININGFILE and writes the
learned model to a MODELFILE passed as arguments. The model is trained for
20 iterations, checks the accuracy on the learned model using the DEVFILE
passed as an optional argument and chooses that model for which accuracy
is the highest.

USAGE: python3 perceplearn.py TRAININGFILE MODELFILE [-h DEVFILE]

Author: Dharmik.
Date: Feb 24th, 2015.

"""

import copy
import getopt
import json
import random
import sys

                 
def get_class(weights, word_list, default_cls):
    """
    Calulates the score for each class the word_list can belong to
    and return that class with maximum score.
    @param weights: Dict containing the weights of each feature for each class.
    @param word_list: List of words(n-gram) for which we have to calculate the class.
    @return: class to which the word belongs.
    """
    scores = {}
    for w in word_list:
        for cls in weights.get(w,[]):
            if cls not in scores:
                scores[cls] = weights[w][cls]
            else:
                scores[cls] += weights[w][cls]
    max_score = 1
    max_cls = default_cls
    for cls in scores:
        if max_score <= scores[cls]:
            max_score = scores[cls]
            max_cls = cls
    return max_cls

def lazy_add_dicts(d1, d2, last_update, present, prev_weight):
    """
    Add the contents of dict-2 to dict-1 lazily.
    If last_update = 0, then there are no lazy updates,
    else, we need to add the value of prev_weight to dict-1
    (present-last_update) no. of times.
    """
    if last_update == 0:
        for cls in d2:
            d1[cls] = d2[cls]
    else:
        lazy_updates = present - last_update
        for cls in d1:
            if cls in prev_weight:
                d1[cls] += lazy_updates*prev_weight[cls]
            if cls in d2:
                d1[cls] += d2[cls]
    for cls in d2:
        if cls not in d1:
            d1[cls] =d2[cls]


def train_perceptron(train_data, weights, weights_avg,        
                     last_update, changes_to_dict, default_cls):
    """
    This method learns the perceptron for each feature in the training data
    for each class.
    @param train_data: List of lists of features.
    @return: Dict data struture containing the weights of each feature for each class.
    """
    for line in train_data:
        changes_to_dict += 1
        cls = line[0]
        features = line[1:]
        cal_cls = get_class(weights, features, default_cls)
        # If calculated class is not as desired class,
        # change the weights accordingly for these classes.
        if cls != cal_cls:
            for w in features:
                if w not in weights:
                    weights[w] = {}
                prev_weights = copy.deepcopy(weights[w])
                if cls not in weights[w]:
                    weights[w][cls] = 1
                else:
                    weights[w][cls] += 1
                if cal_cls not in weights[w]:
                    weights[w][cal_cls] = -1
                else:
                    weights[w][cal_cls] += -1
                lazy_add_dicts(weights_avg.setdefault(w,{}), weights[w],
                               last_update.get(w, 0), changes_to_dict, prev_weights)
                # Update the last update time of this word.
                last_update[w] = changes_to_dict
    # Update all the weights in average perceptron after end of iteration.
    for x in weights:
        lazy_add_dicts(weights_avg.setdefault(x,{}), {},
                       last_update.get(x, 0), changes_to_dict, weights[x])


def get_dev_accuracy(weights, dev_file, default_cls):
    if dev_file is None or dev_file == "":
        return 0
    f_dev = open(dev_file,"r", encoding="latin-1")
    total_count = 0
    correct = 0
    for line in f_dev.readlines():
        total_count += 1
        line_content = line.strip().split(" ")
        pred_tag = get_class(weights, line_content[1:], default_cls)
        if pred_tag == line_content[0]:
            correct += 1
    return float(correct)/ float(total_count)


def train_test_perceptron(train_data, dev_file, default_cls):
    weights = {}
    weights_avg = {}
    last_update = {}
    changes_to_dict = 0
    max_accuracy = -1
    good_weights_avg = weights_avg
    for run in range(20):
        weights = {}
        # Train the perceptron.
        train_perceptron(train_data, weights, weights_avg, last_update,
                         changes_to_dict, default_cls)
        
        # Get the accuracy on dev data.
        cur_accuracy = get_dev_accuracy(weights_avg, dev_file, default_cls)
        print (run, cur_accuracy)
        if cur_accuracy >= max_accuracy:
            max_accuracy = cur_accuracy
            good_weights_avg = copy.deepcopy(weights_avg)
        
        # Shuffle the dev data set.
        random.shuffle(train_data)
    print ("Max = ", str(max_accuracy))
    return good_weights_avg
               

def get_frequent_cls(train_data):
    cls_count = {}
    for line in train_data:
        if line[0] in cls_count:
            cls_count[line[0]] += 1
        else:
            cls_count[line[0]] = 1
            
    # Get the cls with highest count.
    max_count = -1
    for k,v in cls_count.items():
        if max_count < v:
            cls = k
            max_count = v
    return cls

def get_train_data(input_file):
    training_data = []
    with open(input_file, "r", encoding="latin-1") as f_in:
        for line in f_in.readlines():
            line_tags = line.strip().split(" ")
            if len(line_tags) <= 0:
                continue
            training_data.append(line_tags)
    print ("No. of lines in training data = ", len(training_data))
    return training_data

def main(input_file, output_file, argv):
    try:
        opts, args = getopt.getopt(argv, "h:", ["dev="])
    except getopt.GetoptError:
        print ("USAGE: python3 perceptlearn.py <TRAININGFILE> <MODELFILE> [-h DEVFILE]")
        exit()
    dev_file = None
    for opt, arg in opts:
        if opt in ("-h", "--dev"):
            dev_file = arg
            
    # Read the input file.
    train_data = get_train_data(input_file)
    
    # Get the most frequent class from training set as default class.
    default_cls = get_frequent_cls(train_data)
    
    # Train the perceptrons.
    weights_avg = train_test_perceptron(train_data, dev_file, default_cls)
    
    # Write to a model file.
    f_op = open(output_file, "w")
    json_data = json.dumps(weights_avg)
    f_op.write(json_data + "\n")
    f_op.write(default_cls)
    f_op.close()


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3:])
