"""
This file formats the given POS (Parts of Speech) training file TRAININGFILE
to a format which can be processed by perceplearn.py to train the
perceptrons and writes the model learned to a MODEL file specified.

USAGE: python3 postrain.py TRAININGFILE MODEL [-h DEVFILE]

Author: Dharmik.
Date: Feb 22, 2015.
"""
import getopt
import re
import sys
from subprocess import call
from os.path import basename

def get_word_shape(word):
    """
    Replace all lower case characters with a, all upper case characters with A,
    all digits with 9, and all non-alpha characters with -.
    Replace similar continuous letters with single letter of the same type.
    """
    word = re.sub("[a-z]", "a", word)
    word = re.sub("[A-Z]", "A", word)
    word = re.sub("[1-9]", "1", word)
    word = re.sub("[^A-Za-z1-9]", "-", word)
    word = re.sub("(a+)", "a", word)
    word = re.sub("(A+)", "A", word)
    word = re.sub("(1+)", "1", word)
    word = re.sub("(-+)", "-", word)
    return word

def format_training_file(train_file):
    f_in = open(train_file, "r",errors="ignore")
    fmtd_output = "fmtd_" + basename(train_file)
    f_out = open(fmtd_output, "w")
    for line in f_in.readlines():
        line_content = line.strip().split(" ")
        line_content.insert(0, "BOS/BOS")
        line_content.append("EOS/EOS")
        for i in range(1, len(line_content) - 1):
            prev_w, prev_tag = line_content[i-1].strip().rsplit("/", 1)
            cur_w, cur_tag = line_content[i].strip().rsplit("/", 1)
            next_w, next_tag = line_content[i+1].strip().rsplit("/", 1)
            #f_out.write(cur_tag + " cur_" + cur_w + " prev_" + prev_w + " next_" + next_w + "\n")
            #" prev_" + prev_w +
            f_out.write(cur_tag + " cur_" + cur_w + " prev_" + prev_w +
                        " next_" + next_w + " prevtag_" + prev_tag + 
                        " shape_" + get_word_shape(cur_w) + "\n")
    f_out.close()
    f_in.close()
    return fmtd_output
    

def main(train_file, model_file, argv):
    try:
        opts, args = getopt.getopt(argv, "h:", ["dev="])
    except getopt.GetoptError:
        print ("USAGE: python3 postrain.py TRAININGFILE MODEL [-h DEVFILE]")
        exit()
    dev_file = None
    for opt, arg in opts:
        if opt in ("-h", "--dev"):
            dev_file = arg
            
    # Format the POS training file to feed to perceptron.
    fmtd_train_file = format_training_file(train_file)
    
    # Format the dev file, if available.
    opt_dev_file_arg = ""
    if dev_file is not None:
        fmtd_dev_file = format_training_file(dev_file)
        opt_dev_file_arg = " -h " + fmtd_dev_file
    
    # Run the perceplearn on the formatted training file.
    try:
        retcode = call("python3 ../perceplearn.py " + fmtd_train_file +
                       " " + model_file + opt_dev_file_arg, shell=True)
        if retcode < 0:
            print ("Child was terminated by signal-", retcode)
            
    except OSError as e:
        print ("Execution of perceplearn failed:", e)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print ("USAGE: python3 postrain.py TRAININGFILE MODEL [-h DEVFILE]")
        exit()
    main(sys.argv[1], sys.argv[2], sys.argv[3:])
